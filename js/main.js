//1 Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування

/* Экранирование символов — замена в тексте управляющих символов
     на соответствующие текстовые подстановки */

//2 Які засоби оголошення функцій ви знаєте?

/* function, стрелочная функцыя, функцыи конструкторы,
     новая функцыя, генераторы, именные функцыи */

//3 Що таке hoisting, як він працює для змінних та функцій?

/* это, когда переменные и объявленыи
     функции поднимаются вверх по своей области видимости перед выполнением кода(?наверное) */

//-------------------------------------------------------------------------------------------------

// Создаём переменные для ввода данных

let firstName = prompt("Enter your name");
let lastName = prompt("Enter your lastname");
let birthdate = prompt("Enter your birthdate in the format dd.mm.yyyy");

// Создаём функцыю в нутри которой будет обьект

function createNewUser() {
  let newUser = {
    firstName: firstName,
    lastName: lastName,
    birthday: birthdate,

    getLogin: function () {
      return (this.firstName[0], this.lastName).toLowerCase();
    },
    // Создаём медот Age для счёта возвраста

    getAge: function () {
      let today = new Date();
      let birthdateArray = this.birthday.split(".");
      let birthdateObj = new Date(
        birthdateArray[2],
        birthdateArray[1] - 1,
        birthdateArray[0]
      );
      let age = today.getFullYear() - birthdateObj.getFullYear();
      let month = today.getMonth() - birthdateObj.getMonth();

      // Делаем проверку счёта возвраста

      if (
        month < 0 ||
        (month === 0 && today.getDate() < birthdateObj.getDate())
      ) {
        age--;
      }
      return age;
    },

    // Создаём метод для password и делаем возврат

    getPassword: function () {
      let firstInitial = this.firstName.charAt(0).toUpperCase();
      let lastNameLower = this.lastName.toLowerCase();
      let birthdateArray = this.birthday.split(".");
      let year = birthdateArray[2];

      return firstInitial + lastNameLower + year;
    },
  };

  return newUser;
}

// Создаём нового пользователя и выводи результаты в консоль

let user = createNewUser();

console.log(user);
console.log(user.getLogin());
console.log("Age", user.getAge());
console.log("Password", user.getPassword());
